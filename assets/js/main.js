(function ($, Drupal, drupalSettings) {
  "use strict";
  Drupal.behaviors.accessibility_block = {
    attach: function (context, settings) {
      document.querySelectorAll(".acc-tools-click").forEach((element) => {
        element.addEventListener("click", (evt) => {
          evt.preventDefault();
          document.querySelector(".acc-tools").classList.add("active");
        });
      });
      document.querySelectorAll(".acc-tools-close").forEach((element) => {
        element.addEventListener("click", (evt) => {
          evt.preventDefault();
          document.querySelector(".acc-tools").classList.remove("active");
        });
      });
      document.querySelector(".acc-tools").addEventListener("click", (evt) => {
        if (!evt.target.closest(".acc-tools-container")) {
          document.querySelector(".acc-tools").classList.remove("active");
        }
      });
      // Dark Mode Block
      document.querySelectorAll(".greyscale-click").forEach((element) => {
        element.addEventListener("click", (evt) => {
          evt.preventDefault();
          localStorage.setItem("appearance", "greyscale");
          document.documentElement.classList.add("greyscale");
          element.classList.add("active");
          document.querySelector(".colored-click").classList.remove("active");
        });
      });
      document.querySelectorAll(".colored-click").forEach((element) => {
        element.addEventListener("click", (evt) => {
          evt.preventDefault();
          localStorage.setItem("appearance", "colored");
          document.documentElement.classList.remove("greyscale");
          element.classList.add("active");
          document.querySelector(".greyscale-click").classList.remove("active");
        });
      });
      const darkModeState = localStorage.getItem("appearance") || null;
      if (darkModeState === "greyscale") {
        document.documentElement.classList.add("greyscale");
        document.querySelector(".greyscale-click").classList.add("active");
        document.querySelector(".colored-click").classList.remove("active");
      } else {
        document.documentElement.classList.remove("greyscale");
        document.querySelector(".colored-click").classList.add("active");
        document.querySelector(".greyscale-click").classList.remove("active");
      }
      // Font Resize Block

      const FontSize = localStorage.getItem("FontSize");
      changeSize(FontSize)
      document.querySelectorAll(".font-resize-menu a").forEach((element) => {
      if (FontSize != null  && FontSize == element.getAttribute("targetFor"))
        element.classList.add("active");
      });
      document.querySelectorAll(".font-resize-menu a").forEach((element) => {
        element.addEventListener("click", (evt) => {
          evt.preventDefault();
          document.querySelectorAll(".font-resize-menu a").forEach((el) => {
            el.classList.remove("active");
            const FontSize = localStorage.getItem("FontSize");
            document.documentElement.classList.remove(FontSize);
          });
          localStorage.setItem("FontSize", element.getAttribute("targetFor"));
          element.classList.add("active");
          const Size = element.getAttribute("targetFor");
          changeSize(Size)
        });
      });
      function changeSize(Size){
        if (Size == "accessibility_small") {
          document.body.style.fontSize =
            drupalSettings.accessibility.accessibility_small.concat("px");
          const p_element = document.querySelectorAll("p");
          p_element.forEach((p_element) => {
            p_element.style.fontSize =
              drupalSettings.accessibility.accessibility_small.concat("px");
          });
        }
        if (Size == "accessibility_medium") {
          document.body.style.fontSize =
            drupalSettings.accessibility.accessibility_medium.concat("px");
          const p_element = document.querySelectorAll("p");
          p_element.forEach((p_element) => {
            p_element.style.fontSize =
              drupalSettings.accessibility.accessibility_medium.concat("px");
          });
        }
        if (Size == "accessibility_large") {
          document.body.style.fontSize =
            drupalSettings.accessibility.accessibility_large.concat("px");
          const p_element = document.querySelectorAll("p");
          p_element.forEach((p_element) => {
            p_element.style.fontSize =
              drupalSettings.accessibility.accessibility_large.concat("px");
          });
        }
        if (Size == "accessibility_very_large") {
          document.body.style.fontSize =
            drupalSettings.accessibility.accessibility_very_big.concat("px");
          const p_element = document.querySelectorAll("p");
          p_element.forEach((p_element) => {
            p_element.style.fontSize =
              drupalSettings.accessibility.accessibility_very_big.concat(
                "px"
              );
          });
        }
        document.documentElement.classList.add(Size);
      };
    },
  };
})(jQuery, Drupal, drupalSettings);
