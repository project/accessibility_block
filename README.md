# Accessibility Tools block

Accessibility Tools block is a Drupal module that provides a custom block with features for resizing fonts and enabling dark mode on your Drupal site.

## Installation

1. Download and install the module like any other Drupal module.
2. Enable the "Accessibility Tools block" module on the Extend page (Administer -> Extend).

## Usage

1. Go to the Blocks layout page (Administer -> Structure -> Block layout).
2. Find the "My Custom Block" in the list of available blocks.
3. Place the block in the desired region of your site.
4. Configure the block settings to customize the font resizing and dark mode options.

## Block Configuration

The "Accessibility Tools block" comes with the following configuration options:

- **Font Size**: Adjust the font size for your site. Enter a numeric value in pixels.
- **Dark Mode**: Enable or disable the dark mode feature. When enabled, the site will be displayed with a dark color scheme.

To configure the block settings, follow these steps:

1. Navigate to the Blocks layout page (Administer -> Structure -> Block layout).
2. Find the "Accessibility Tools block" and click on the "Configure" link next to it.
3. Adjust the font size and dark mode settings according to your preference.
4. Save the changes.

## Theme Modifications

In some cases, you may need to make theme modifications to fully support font resizing and dark mode. Please consult your theme's documentation for instructions on making these changes.
