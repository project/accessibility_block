<?php

namespace Drupal\accessibility_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Accessibility Tools block' block.
 *
 * @Block(
 *  id = "accessibility_block",
 *  admin_label = @Translation("Accessibility Tools Block"),
 * )
 */
class AccessibilityTools extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $dark_mode = $config['dark_mode'] ?? FALSE;
    $font_resize = $config['font_resize'] ?? FALSE;
    $accessibility_small = $config['accessibility_small'] ?? FALSE;
    $accessibility_medium = $config['accessibility_medium'] ?? FALSE;
    $accessibility_large = $config['accessibility_large'] ?? FALSE;
    $accessibility_very_big = $config['accessibility_very_big'] ?? FALSE;
    $module_path = \Drupal::service('extension.list.module')->getPath('accessibility_block');
    return [
      '#theme' => 'accessibility_block',
      '#attached' => [
        'library' => [
          'accessibility_block/accessibility',
        ],
        'drupalSettings' => [
          'accessibility' => [
            'accessibility_small' => $accessibility_small,
            'accessibility_medium' => $accessibility_medium,
            'accessibility_large' => $accessibility_large,
            'accessibility_very_big' => $accessibility_very_big,
          ],
        ],
      ],
      'class' => [
        'accessibility_tools_block',
      ],
      '#dark_mode' => $dark_mode,
      '#font_resize' => $font_resize,
      '#accessibility_small' => $accessibility_small,
      '#accessibility_medium' => $accessibility_medium,
      '#accessibility_large' => $accessibility_large,
      '#accessibility_very_big' => $accessibility_very_big,
      '#module_path' => $module_path,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $form['dark_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Dark Mode'),
      '#default_value' => $config['dark_mode'] ?? FALSE,
    ];

    $form['font_resize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Font Resize'),
      '#default_value' => $config['font_resize'] ?? FALSE,
    ];

    $form['accessibility_small'] = [
      '#type' => 'number',
      '#title' => $this->t('Small Font Size'),
      '#default_value' => $config['accessibility_small'] ?? FALSE,

    ];
    $form['accessibility_medium'] = [
      '#type' => 'number',
      '#title' => $this->t('Medium Font Size'),
      '#default_value' => $config['accessibility_medium'] ?? FALSE,
    ];
    $form['accessibility_large'] = [
      '#type' => 'number',
      '#title' => $this->t('Large Font Size'),
      '#default_value' => $config['accessibility_large'] ?? FALSE,
    ];
    $form['accessibility_very_big'] = [
      '#type' => 'number',
      '#title' => $this->t('X-Large Font Size'),
      '#default_value' => $config['accessibility_very_big'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['dark_mode'] = $values['dark_mode'];
    $this->configuration['font_resize'] = $values['font_resize'];
    $this->configuration['accessibility_small'] = $values['accessibility_small'];
    $this->configuration['accessibility_medium'] = $values['accessibility_medium'];
    $this->configuration['accessibility_large'] = $values['accessibility_large'];
    $this->configuration['accessibility_very_big'] = $values['accessibility_very_big'];
  }

}
